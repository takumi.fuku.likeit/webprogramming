

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // セッション喪失時ログイン画面に遷移
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータ取得
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDay = request.getParameter("birth-date");

      // パスワードミス、未入力の場合
      if ((!password.equals(passwordConfirm)) || (loginId.equals("")) || (password.equals(""))
          || (name.equals("")) || (birthDay.equals(""))) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birthDay", birthDay);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      UserDao dao = new UserDao();
      // ログインIDが既存のものかの確認
      List<User> u = dao.findAll();
      for (User user : u) {
        if (loginId.equals(user.getLoginId())) {
          request.setAttribute("errMsg", "入力された内容は正しくありません");
          request.setAttribute("name", name);
          request.setAttribute("birthDay", birthDay);
          RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userAdd.jsp");
          dispatcher.forward(request, response);
          return;
        }
      }

      password = PasswordEncorder.encordPassword(password);
      Date birthDate = Date.valueOf(birthDay);

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      dao.userAdd(loginId, password, name, birthDate);

      response.sendRedirect("UserListServlet");



	}

}
