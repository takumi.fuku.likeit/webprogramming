

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    // TODO userList.jspのフォーム制御
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // セッション喪失時ログイン画面に遷移
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // 一覧の取得
      UserDao userdao = new UserDao();
      List<User> userList = userdao.findAll();

      // リクエストスコープにセット
      request.setAttribute("userList", userList);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // 文字コード
      request.setCharacterEncoding("UTF-8");

      // フォームの受け取り
      String loginId = request.getParameter("user-loginid");
      String name = request.getParameter("user-name");
      String date1 = request.getParameter("date-start");
      String date2 = request.getParameter("date-end");

      // 検索
      UserDao dao = new UserDao();
      List<User> userList = dao.searchUser(loginId, name, date1, date2);

      // リクエストスコープにセット
      request.setAttribute("loginId", loginId);
      request.setAttribute("userName", name);
      request.setAttribute("dateStart", date1);
      request.setAttribute("dateEnd", date2);
      request.setAttribute("userList", userList);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);

	}

}
