

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // セッション喪失時ログイン画面に遷移
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // IDを取得
      String i = request.getParameter("id");
      int id = Integer.valueOf(i);

      // IDを引数にして情報を取得
      UserDao dao = new UserDao();
      User user = dao.findUserData(id);

      // ユーザーデータをリクエストスコープにセット
      request.setAttribute("loginId", user.getLoginId());
      request.setAttribute("userName", user.getName());
      request.setAttribute("birthDate", user.getBirthDate());
      request.setAttribute("createDate", user.getCreateDate());
      request.setAttribute("updateDate", user.getUpdateDate());

      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDetail.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
