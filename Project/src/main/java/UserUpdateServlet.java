

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // セッション喪失時ログイン画面に遷移
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String i = request.getParameter("id");
      int id = Integer.valueOf(i);
      UserDao dao = new UserDao();
      User user = dao.findUserData(id);

      request.setAttribute("id", user.getId());
      request.setAttribute("loginId", user.getLoginId());
      request.setAttribute("name", user.getName());
      request.setAttribute("birthDay", user.getBirthDate());

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // 文字コード
      request.setCharacterEncoding("UTF-8");

      // ID受け取り
      String i = request.getParameter("id");
      int id = Integer.valueOf(i);

      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDay = request.getParameter("birth-date");

      UserDao dao = new UserDao();
      // パスワードと確認パスワードが空欄だった場合
      if (password.equals("") && passwordConfirm.equals("")) {
        User user = dao.findUserData(id);
        password = user.getPassword();
      }

      /*
       * 確認パスワードが間違っていた場合の処理 パスワード以外に未入力の項目があった場合 更新失敗時入力画面に移動。詳細は仕様書確認
       */
      else if (password.equals(passwordConfirm) == false || name.equals("")
          || birthDay.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        User user = dao.findUserData(id);
        request.setAttribute("loginId", user.getLoginId());
        request.setAttribute("name", name);
        request.setAttribute("birthDay", birthDay);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      password = PasswordEncorder.encordPassword(password);
      Date birthDate = Date.valueOf(birthDay);

      dao.userUpdate(id, password, name, birthDate);

      response.sendRedirect("UserListServlet");
	}

}
