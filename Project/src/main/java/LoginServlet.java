

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッション保持時ユーザー一覧にリダイレクト
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u != null) {
        response.sendRedirect("UserListServlet");
        return;
      }

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // 文字コード
      request.setCharacterEncoding("UTF-8");

      // 受け取り
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");

      password = PasswordEncorder.encordPassword(password);

      UserDao dao = new UserDao();
      User user = dao.findUserInfo(loginId, password);

      // userがnullで渡された場合
      if (user == null) {
        request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
        request.setAttribute("loginId", loginId);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // テーブル内でデータが参照できた場合
      HttpSession session = request.getSession();
      session.setAttribute("userInfo", user);

      response.sendRedirect("UserListServlet");

	}

}
