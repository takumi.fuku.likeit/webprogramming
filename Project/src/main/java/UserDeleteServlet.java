

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // セッション喪失時ログイン画面に遷移
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String i = request.getParameter("id");
      int id = Integer.valueOf(i);
      UserDao dao = new UserDao();
      User user = dao.findUserData(id);

      request.setAttribute("user", user);

      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ID受け取り
      String i = request.getParameter("user-id");
      int id = Integer.valueOf(i);

      UserDao dao = new UserDao();
      dao.userDelete(id);

      response.sendRedirect("UserListServlet");
	}

}
