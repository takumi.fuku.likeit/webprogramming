package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {
  public User findUserInfo(String loginId, String password) {
    Connection conn = null;
    User user;

    try {
      // 接続
      conn = DBManager.getConnection();

      // SELECT文
      String sql = "SELECT * FROM user WHERE login_id = ? AND password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");

      user = new User(loginIdData, nameData);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return user;
  }


  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String spl = "SELECT * FROM user WHERE is_admin = false";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(spl);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }


  public void userAdd(String loginId, String password, String name, Date birthDate) {
    Connection conn = null;

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "INSERT INTO user VALUES (0, ?, ?, ?, ?, 0, NOW(), NOW())";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setDate(3, birthDate);
      pStmt.setString(4, password);

      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }


  }



  public User findUserData(int id) {
    Connection conn = null;
    User user = null;
    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int idData = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      user = new User(idData, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return user;

  }



  public void userUpdate(int id, String password, String name, Date birthDate) {
    Connection conn = null;

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql =
          "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = NOW() WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, password);
      pStmt.setString(2, name);
      pStmt.setDate(3, birthDate);
      pStmt.setInt(4, id);

      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public void userDelete(int id) {
    Connection conn = null;

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM user WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setInt(1, id);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public List<User> searchUser(String loginId, String name, String birthDate, String birthDate2) {
    Connection conn = null;
    List<User> searchList = new ArrayList<User>();
    List<String> strList = new ArrayList<String>();
    List<Date> dateList = new ArrayList<Date>();

    try {
      // 接続
      conn = DBManager.getConnection();

      StringBuilder sql = new StringBuilder();
      sql.append("SELECT * FROM user WHERE is_admin = false");

      if (loginId.equals("") == false) {

        sql.append(" AND login_id = ?");
        strList.add(loginId);

      }
      if (name.equals("") == false) {

        sql.append(" AND name LIKE ?");
        strList.add("%" + name + "%");

      }
      if (birthDate.equals("") == false) {

        sql.append(" AND birth_date >= ?");
        Date dateStart = Date.valueOf(birthDate);
        dateList.add(dateStart);

      }
      if (birthDate2.equals("") == false) {

        sql.append(" AND birth_date <= ?");
        Date dateEnd = Date.valueOf(birthDate2);
        dateList.add(dateEnd);

      }

      PreparedStatement pStmt = conn.prepareStatement(sql.toString());

      int i = 1;
      for (String str : strList) {

        pStmt.setString(i, str);
        i++;

      }

      int n = strList.size();
      for (Date date : dateList) {

        n++;
        pStmt.setDate(n, date);

      }


      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String userLoginId = rs.getString("login_id");
        String userName = rs.getString("name");
        Date userBirthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        User user = new User(id, userLoginId, userName, userBirthDate, password, isAdmin,
            createDate, updateDate);
        searchList.add(user);
      }


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return searchList;

  }

}
