﻿--データベース
CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
--宣言
USE usermanagement;

--テーブル
CREATE TABLE user (
    id SERIAL PRIMARY KEY AUTO_INCREMENT COMMENT 'ID',
    login_id varchar(255) UNIQUE Not Null COMMENT 'ログインID',
    name varchar(255) Not Null COMMENT '名前',
    birth_date DATE Not Null COMMENT '生年月日',
    password varchar(255) Not Null COMMENT 'パスワード',
    is_admin boolean Not Null DEFAULT false COMMENT '管理者フラグ',
    create_date DATETIME Not Null COMMENT '作成日時',
    update_date DATETIME Not Null COMMENT '更新日時'
    )


--管理者
INSERT INTO user VALUES (
    0,
    'admin',
    '管理者',
    '2000-01-01',
    'password',
    1,
    NOW(),
    NOW()
    )
